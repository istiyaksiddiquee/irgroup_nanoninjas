Over the last two weeks, in sworn testimony from experienced public servants with no political axes to grind, the American people have learned that President Trump orchestrated a scheme to extract what he called a “favor” from a foreign leader by withholding a White House meeting and hundreds of millions of dollars in military aid, against his own administration’s policy and the bipartisan wishes of Congress.

And yet the details of the Ukraine story — involving veiled threats, Latin phrases, less genteel “Trumpspeak” and “irregular channels” of diplomacy — don’t map neatly onto some Americans’ idea of obvious wrongdoing. One nagging question for many is whether Mr. Trump was really doing all this for himself, rather than in pursuit of the American national interest. It’s a crucial question; in fact, it’s at the heart of the inquiry.

After all, there’s nothing wrong with conditioning foreign aid on compliance with established foreign policy goals. But that’s not what Mr. Trump did. To the contrary, every known piece of evidence offered so far points in the other direction.

Sign up for David Leonhardt's newsletter
David Leonhardt helps you make sense of the news — and offers reading suggestions from around the web — with commentary every weekday morning.

SIGN UP
To list just a few: Rudy Giuliani, who was behind the Ukraine operation, has said publicly that he was seeking investigations damaging to Mr. Trump’s political rival in his capacity as Mr. Trump’s personal lawyer, and to advance Mr. Trump’s personal interests.

DEBATABLEAgree to disagree, or disagree better? Broaden your perspective with sharp arguments on the most pressing issues of the week. Sign up here.
Multiple impeachment witnesses have testified that Mr. Trump did not care about systemic corruption in Ukraine, a longstanding focus of American foreign policy, including in the Trump administration. On Thursday, David Holmes, a career diplomat working in the embassy in Kyiv, testified that Gordon Sondland, the ambassador to the European Union, agreed that Mr. Trump didn’t care about Ukraine and that he only cared about “big stuff,” like investigating the Bidens.

Then there’s the fact that, as noted in the Lawfare blog, Mr. Trump approved military aid to Ukraine in 2017 and 2018, even though Hunter Biden’s role as a director of the Ukrainian gas company Burisma was well-known at the time. Mr. Trump and his Republican allies now say that it’s in the national interest to get to the bottom of how it could be that Hunter Biden was serving for five years, at great financial benefit to himself, on the Burisma board.

This page was worried about that question in 2015. It’s interesting that Mr. Trump didn’t become fixated on it until 2019. What changed this year? Well, Hunter’s father, Joe Biden, became a presidential candidate.

And of course, there is the summary of Mr. Trump’s July 25 “perfect” call with Ukrainian President Volodymyr Zelensky. Despite Mr. Trump’s exhortations that Americans “read the transcript,” it is not exculpatory. In fact, it provides the most direct evidence to date that Mr. Trump was seeking a bribe: When Mr. Zelensky brought up the military aid, Mr. Trump appeared to condition it (“do us a favor though”) on the announcement of investigations into Ukraine’s supposed interference in the 2016 election and the purported corruption of Mr. Biden and his son.

Editors’ Picks

The Jungle Prince of Delhi

Typing These Two Letters Will Scare Your Young Co-Workers

The 10 Best Books of 2019
Not direct enough for you? Mr. Trump made it easier about a week after the release of the call summary. Speaking to reporters on the White House lawn last month, the president called on China to investigate the Bidens, too. He added, ominously, “if they don’t do what we want, we have tremendous power.”

Mr. Trump’s more honest defenders don’t deny the basic story here. Instead, they argue that soliciting help from a foreign government for personal political gain is just not that bad. Mr. Trump’s actions regarding Ukraine were “inappropriate” and “not how the executive should handle such things,” said Representative Will Hurd of Texas, but he shouldn’t be impeached for them. That’s far too glib a judgment. The nation’s founders made that clear by listing bribery as one of just two specific offenses meriting impeachment.

At the constitutional convention in 1787, Gouverneur Morris agreed that impeachment was a tool Congress needed to deal with a corrupt chief executive. “He may be bribed by a greater interest to betray his trust, and no one would say that we ought to expose ourselves to the danger of seeing the first Magistrate in foreign pay, without being able to guard against it by displacing him,” Morris said. “This Magistrate is not the King but the prime minister. The people are the King.”

Another framer, George Mason, asked, “Shall any man be above justice?” He added, “Shall the man who has practiced corruption, and by that means procured his appointment in the first instance, be suffered to escape punishment by repeating his guilt?”

Nearly a decade later, in his farewell address to the nation, George Washington warned Americans “to be constantly awake, since history and experience prove that foreign influence is one of the most baneful foes of republican government.”

Why were the framers so concerned about bribery and foreign influence? Because they had plenty of evidence of the damage it could do. They were designing the world’s first attempt at large-scale republican self-government, and they knew its success, and even survival, would depend on elected leaders who represented the people’s interest, not their own.

In other words, Americans agree to give their elected officials power over them, and those officials agree to exercise that power on Americans’ behalf. If the nation’s leaders breach that deal by lining their own pockets and bartering the interests of their citizens, they break the trust that self-government and democracy depend on. The testimony so far indicates that it’s even worse in this case. It suggests that Mr. Trump wasn’t simply soliciting a bribe, but doing so to try to rig the next election. It should go without saying that representative democracy cannot work if its leaders are cheating to keep themselves in power.

The argument that there’s nothing to worry about because Mr. Trump’s Ukraine scheme didn’t work in the end misses the point. If Mr. Trump is allowed to get away with this blatant attempt at subverting the will of the 2020 voters, what’s to stop him from trying again? Remember, the phone call with the Ukrainian president, which is at the heart of the impeachment inquiry, happened just one day after the special counsel Robert Mueller testified in Congress about the end of the Russia investigation. If the House of Representatives impeaches Mr. Trump and then the Senate acquits him, it’s reasonable to assume that he would take that outcome as exoneration — and as carte blanche to do whatever he wants to win in 2020.

Throughout the hearings, House Democrats have done their job as representatives of a coequal branch of government, attempting to get to the bottom of grave allegations of wrongdoing by the president. Representative Adam Schiff, the chairman of the House Intelligence Committee, has behaved with admirable focus and restraint as his Republican colleagues have acted like children, yelling at witnesses and leaving the room when they didn’t want to hear damaging testimony.

There could well be more to come, too, if the many administration officials currently refusing to appear do the right thing and show up. Many have sent out news releases disputing witness testimony. Those witnesses were willing to testify under oath, however, at risk of perjury — and as yet Mr. Trump’s defenders have not been. If they don’t have anything to hide, why not come forward and speak under oath?

Republicans complain that this is all a partisan attack, or a “hoax,” as the president calls it. There’s no question that Democrats have their own partisan interests. But that’s no excuse for the Republicans to ignore all this evidence. In conducting this inquiry, the Democrats are doing what people concerned about protecting the nation would do. It is the Republicans who are turning this process into a partisan dogfight, attacking lifelong public servants, implying they have dual loyalty and misstating testimony they heard the day before. Rather than listening to what witnesses were telling them, some of them chose to pound the table and regurgitate conspiracy theories about Ukraine that were long ago debunked by American intelligence as Russian disinformation and misdirection.

Put this in some perspective: A party that was more than happy to impeach a president for lying about a sexual affair has refused to cast even a single vote in favor of an inquiry into whether to impeach a president who, by the credible accounts of his own appointees, has undermined national security for political advantage.

For the record: This page supported the impeachment inquiry into President Bill Clinton, as it has supported this inquiry into President Trump.

This Republican display of herd instinct is part of the broader unwillingness of the party’s leaders, and their hypocritical supporters in the partisan press, to check Mr. Trump’s most disturbing tendency of all. From the beginning of his term in 2017, President Trump has asserted broad, even monarchical, powers. He can use the extraordinary force of his office to strong-arm other countries into serving as his political pawns. He can run a protection racket with military alliances and pull out of international organizations and treaties. He can profit off the presidency. The Justice Department’s guidelines bar the indictment of a sitting president.

According to his lawyers, he can’t even be investigated, no matter what he has done or might do. Now, he’s being asked to defend his behavior before a coequal branch of the government, an exercise that he chooses, in his kingly manner, to deem illegitimate. So he refuses to participate and stops others from doing so.

In July Mr. Trump said that the Constitution gives him “the right to do whatever I want.” Those are the words of a despot, not an American president. As the impeachment inquiry proceeds, Republicans who claim they have no problem with this sort of talk should ask themselves how they would feel if it were coming from a President Joe Biden.