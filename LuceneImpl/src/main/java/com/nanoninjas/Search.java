package com.nanoninjas;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.nio.file.Paths;
import java.util.logging.Logger;


class Search {

    private final static Logger LOGGER = Logger.getLogger(Search.class.getName());

    void performSearch(String indexDir, String searchString) {
        try {

            System.out.println("performing search operation on the indexed data.");
            System.out.println();

            Directory dir = FSDirectory.open(Paths.get(indexDir));
            IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(dir));
            QueryParser parser = new QueryParser("contents", new StandardAnalyzer());
            Query query = parser.parse(searchString);

            long start = System.currentTimeMillis();
            TopDocs hits = searcher.search(query, 10);
            long end = System.currentTimeMillis();

            System.out.println("Found " + hits.totalHits +
                    " document(s) (in " + (end - start) +
                    " milliseconds) that matched query: \" " + searchString + "\"");
            System.out.println();
            System.out.println();
            System.out.println("Result: ");

            int rankIndex = 0;
            for (ScoreDoc scoreDoc : hits.scoreDocs) {

                Document doc = searcher.doc(scoreDoc.doc);
                System.out.println("Rank " + (++rankIndex) + ": FileName: " + doc.get("fileName") + ", title: " + doc.get("title")
                        + ", relevance score: " + scoreDoc.score + ", path: " + doc.get("filePath")
                        + ", lastModifiedDate: " + doc.get("lastModificationDate") + ", summary: " + doc.get("summary"));

            }

            System.out.println();
            System.out.println();
            dir.close();

        } catch (Exception exception) {
            LOGGER.severe("an exception occurred in Search class.");
        }
    }
}