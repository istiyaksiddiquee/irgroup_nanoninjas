package com.nanoninjas;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;

class RecursivePrinter {

    protected HashMap<String, Information> traverseRecursively(File[] arr, int index, int level, int counter, String pathToResourceFolder, HashMap<String, Information> metaData) {

        // termination condition
        if (index == arr.length) {
            return metaData;
        }

        // tabs for internal levels
        for (int i = 0; i < level; i++) {
            System.out.print("\t");
        }

        // for files
        if (arr[index].isFile()) {

            String fileName = arr[index].getName();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String lastModifiedDate = sdf.format(arr[index].lastModified());

            System.out.println(fileName);

            //The idea of calling tokens function here is to parse, tokenize each file as we're accessing the directory.
            FileTokenizer fileTokenizer = new FileTokenizer();
            String[] returnedValues = fileTokenizer.getTokens(arr[index].getAbsolutePath(), counter++, pathToResourceFolder);

            String tokenFileNameOnly = returnedValues[0];
            String title = returnedValues[1];
            String summary = returnedValues[2];

            Information information = new Information(fileName, arr[index].getAbsolutePath(), title, summary, lastModifiedDate);
            metaData.put(tokenFileNameOnly, information);
        } else if (arr[index].isDirectory() && !arr[index].getName().equals("ninja_tokens")) { // for sub-directories
            System.out.println("[" + arr[index].getName() + "]");
            // recursion for sub-directories
            File[] files = arr[index].listFiles();
            if (files != null && files.length != 0) {
                traverseRecursively(files, 0, level + 1, counter++, pathToResourceFolder, metaData);
            }
        }

        // recursion for main directory
        return traverseRecursively(arr, ++index, level, counter++, pathToResourceFolder, metaData);
    }
}