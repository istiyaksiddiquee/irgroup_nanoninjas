package com.nanoninjas;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Logger;

// The FileTokenizer class tokenizes each file that's passed to it and creates an output tokenized file.
class FileTokenizer {

    private final static Logger LOGGER = Logger.getLogger(FileTokenizer.class.getName());

    private String getSummary(String source) {

        StringBuilder summary = new StringBuilder();

        BreakIterator bi = BreakIterator.getSentenceInstance(Locale.US);
        bi.setText(source);

        int lineCount = 0;
        int lastIndex = bi.first();

        while (lineCount <= 4) {

            int firstIndex = lastIndex;
            lastIndex = bi.next();

            if (lastIndex != BreakIterator.DONE) {
                summary.append(source.substring(firstIndex, lastIndex));
                lineCount++;
            }
        }

        return summary.toString();
    }

    String[] getTokens(String f, int Counter, String pathToResourceFolder) {

        String[] valuesToReturn = new String[3];

        File file = new File(f);
        String path = file.toString();
        StemFileWriter writer = new StemFileWriter();

        // Here we're trying to retrieve teh extension of teh input file so that they can differentiated for tokenization.
        String extension = path.substring(path.lastIndexOf("."), path.length());

        // We're calling the file_path function of class write_class. The actual reason for doing this here is to create an output file
        // mapping to the input file and print the absolute path of the file in the output file. So the first line of every
        // tokenized output file will be path to it's actual input file.
        String[] returnedStrings = writer.filePath(file, Counter, pathToResourceFolder);
        valuesToReturn[0] = returnedStrings[0];
        String tokenFileName = returnedStrings[1];

        if (extension.equals(".txt")) { //Perform below functions if the file type is text
            try {

                valuesToReturn[1] = file.getName();
                valuesToReturn[2] = "";

                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String strLine = " ";
                String fileData = "";

                while ((strLine = bufferedReader.readLine()) != null) {
                    fileData += strLine + " ";
                }
                // String token_file_name;
                // token_file_name = write.file_path(fa,Counter);
                StringTokenizer stringTokenizer = new StringTokenizer(fileData, " !.,-';{}?()");
                while (stringTokenizer.hasMoreTokens()) {
                    String token = stringTokenizer.nextToken();
                    writer.write(token, tokenFileName);
                }
                bufferedReader.close();
            } catch (IOException ioException) {
                LOGGER.severe("an exception has taken place in FileTokenizer class's getTokens() method.");
            }
        } else if (extension.equals(".html")) { //Perform teh below functions if the file type is html

            try {
                //Here we're using Jsoup to parse html files
                Document document = Jsoup.parse(file, "UTF-8");
                valuesToReturn[1] = document.title();
                valuesToReturn[2] = getSummary(document.body().text());

                // Now the first line of every output tokenized file will be the absolute path to it's input file
                //The second line of an html tokenized output file will be the title of the html page.This is done here below.
                writer.write(document.title(), tokenFileName);

                //Now we're trying to retrieve all the body contents of the html page with below statement.
                String body = document.body().text();

                //The below part of teh code will consider teh long string b from above and it'll tokenize each word in the string separated by the
                //delimiters mentioned and will separate it and print it to the output tokenized file.
                StringTokenizer stringTokenizer = new StringTokenizer(body, " !.,-';{}?()");
                while (stringTokenizer.hasMoreTokens()) {
                    String token = stringTokenizer.nextToken();
                    writer.write(token, tokenFileName);
                }
            } catch (IOException ioException) {
                LOGGER.severe("an exception has taken place in FileTokenizer class's getTokens() method.");
            }
        }
        return valuesToReturn;
    }
}