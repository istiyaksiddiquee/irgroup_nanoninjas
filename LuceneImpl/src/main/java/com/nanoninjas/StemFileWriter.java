package com.nanoninjas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

//This is a lovely class which does the hard work of pasting every tokenized/stemmed word in an output file for us.
//Please note each input file maps to one output tokenized/stemmed file.
class StemFileWriter {

    private final static Logger LOGGER = Logger.getLogger(StemFileWriter.class.getName());

    // Now we come to the most hard working part of teh code. The writing function as you can see, receives
    // file name and token to be appended as arguments from who ever is calling it.
    // also one thing to note here is, the tokens are white space  delimited. That means the output tokenized file will have each token
    // separated by a white space.
    void write(String b, String token_file_name) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(token_file_name, true));
            bufferedWriter.append(b + ' ');
            bufferedWriter.close();
        } catch (IOException exception) {
            LOGGER.severe("an exception occurred in StemFileWriter class.");
        }
    }

    // The file_path function just creates
    // 1) Create an Output file with the parameters passed to it.
    // 2) append teh absolute path of teh input file to teh output file
    // 3) Lastly to return the name of the file.
    String[] filePath(File fa, int counter, String pathToResourceFolder) {

        String[] returnValues = new String[2];

        File file = new File(String.valueOf(fa));
        String fileName = counter + file.getName() + ".txt";

        // this is the name of the file with absolute path
        String op = pathToResourceFolder + "\\ninja_tokens\\" + fileName;

        // this was the original output
        File statText = new File(op);

        returnValues[0] = fileName;
        returnValues[1] = statText.toString();

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(statText, true));
            bw.close();
        } catch (IOException e) {
            LOGGER.severe("an exception occurred in StemFileWriter class.");
        }

        return returnValues;
    }
}
