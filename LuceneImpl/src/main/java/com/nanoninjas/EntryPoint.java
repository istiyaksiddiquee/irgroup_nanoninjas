package com.nanoninjas;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

public class EntryPoint {

    private final static Logger LOGGER = Logger.getLogger(EntryPoint.class.getName());

    public static void main(String[] args) {

        String pathToResourceFolder = null;
        String queryString = "lorem ipsum";

        if (args == null || args.length == 0) {
            LOGGER.severe("in order to proceed, please provide path to resource folder");
        } else {

            int counter = 1;
            pathToResourceFolder = args[0];

            RecursivePrinter recursivePrinter = new RecursivePrinter();

            // This is just creating an object linked to the directory so that we can traverse it.
            File mainDirectory = new File(pathToResourceFolder);
            String indexDirectory = pathToResourceFolder + "\\ninja_indexDir";
            String dataDirectory = pathToResourceFolder + "\\ninja_tokens";

            // The counter is a very interesting variable as we're passing this so it counts and appends the value of file number
            // we're performing functions on to the main tokenized file.
            // For example, if data1.txt is the first file we're accessing then it's output tokenized file will be 1data1.txt
            // Now we'll start traversing all the files and folders in the main directory

            if (mainDirectory.exists() && mainDirectory.isDirectory()) {

                // Here we're listing all the files and folders in the main directory
                File[] arr = mainDirectory.listFiles();

                if (new File(dataDirectory).mkdir()) {

                    System.out.println("==============================================");
                    System.out.println("Files from main directory : " + mainDirectory);
                    System.out.println("==============================================");

                    if (arr != null && arr.length != 0) {

                        HashMap<String, Information> metaData = new HashMap<>();

                        // Calling recursive method
                        metaData = recursivePrinter.traverseRecursively(arr, 0, 0, counter, pathToResourceFolder, metaData);

                        System.out.println();
                        System.out.println();

                        // indexing
                        if (new File(indexDirectory).mkdir()) {

                            Indexer.writeIndex(dataDirectory, indexDirectory, metaData);

                            Search search = new Search();
                            search.performSearch(indexDirectory, queryString);

                            System.out.println("cleaning up temporary folders... ");
                            // cleanup
                            try {
                                FileUtils.deleteDirectory(new File(dataDirectory));
                                FileUtils.deleteDirectory(new File(indexDirectory));
                            } catch (IOException ioException) {
                                LOGGER.severe("couldn't cleanup temporary folders properly.");
                            }
                        } else {
                            LOGGER.severe("Insufficient permission to create temporary repository.");
                        }
                    }
                } else {
                    LOGGER.severe("Insufficient permission to create temporary repository.");
                }
            }
        }
    }
}