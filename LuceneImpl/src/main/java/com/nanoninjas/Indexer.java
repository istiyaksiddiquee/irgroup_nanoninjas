package com.nanoninjas;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.logging.Logger;

class Indexer {

    private final static Logger LOGGER = Logger.getLogger(Indexer.class.getName());

    static void writeIndex(String dataDir, String indexDir, HashMap<String, Information> metaData) {
        try {

            Indexer indexerInstance = new Indexer();
            Analyzer analyzer = new StandardAnalyzer();
            Directory dir = FSDirectory.open(Paths.get(indexDir));
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            IndexWriter indexWriter = new IndexWriter(dir, config);
            File dataFolder = new File(dataDir);

            if (dataFolder.isDirectory()) {
                indexerInstance.indexDirectory(indexWriter, dataFolder, metaData);
                System.out.println();
                System.out.println("Number of docs indexed: " + indexWriter.numRamDocs());
                System.out.println();
                System.out.println();
            }

            indexWriter.close();
            dir.close();

        } catch (IOException ioException) {
            LOGGER.severe("an exception has happened in writeIndex method of Indexer class.");
        }
    }

    private void indexDirectory(IndexWriter indexWriter, File dataDirectory, HashMap<String, Information> metaData) throws IOException {
        File[] files = dataDirectory.listFiles();
        for (int i = 0; i < files.length; i++) {
            File f = files[i];
            if (f.isDirectory()) {
                indexDirectory(indexWriter, f, metaData);
            } else {
                indexFileWithIndexWriter(indexWriter, f, metaData);
            }
        }
    }

    private void indexFileWithIndexWriter(IndexWriter indexWriter, File f, HashMap<String, Information> metaData) throws IOException {
        if (f.isHidden() || f.isDirectory() || !f.canRead() || !f.exists()) {
            return;
        }


        Information fileInformation = metaData.get(f.getName());
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer(EnglishAnalyzer.ENGLISH_STOP_WORDS_SET);
        TokenStream ts = new PorterStemFilter(standardAnalyzer.tokenStream("field", new FileReader(f)));
        System.out.println("Indexing file: " + fileInformation.getFilePath());

        Document document = new Document();
        document.add(new TextField("fileName", fileInformation.getFileName(), Field.Store.YES));
        document.add(new TextField("filePath", fileInformation.getFilePath(), Field.Store.YES));
        document.add(new TextField("lastModificationDate", fileInformation.getLastModifiedDate(), Field.Store.YES));
        document.add(new TextField("title", fileInformation.getTitle(), Field.Store.YES));
        document.add(new TextField("summary", fileInformation.getSummary(), Field.Store.YES));
        document.add(new TextField("contents", ts));

        indexWriter.addDocument(document);
    }
}
