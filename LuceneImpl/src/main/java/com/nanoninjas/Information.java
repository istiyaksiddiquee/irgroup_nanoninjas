package com.nanoninjas;

public class Information {

    private String filePath;
    private String fileName;
    private String lastModifiedDate;
    private String title;
    private String summary;

    public Information() {
    }

    public Information(String fileName, String filePath, String title, String summary, String lastModifiedDate) {
        this.filePath = filePath;
        this.fileName = fileName;
        this.title = title;
        this.summary = summary;
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
